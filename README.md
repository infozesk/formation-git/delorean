# PROJET DELOREAN

- ELEVE1: PRENOM NOM, prenom.nom@monmail.com
- ELEVE2: PRENOM NOM, prenom2.nom2@monmail.com

1. Mettre vos noms ci dessus

Le projet pour le TP Git.
Ce projet suit les conventions de branches de [NVIE](http://nvie.com/posts/a-successful-git-branching-model/).

## Organisation

Un dossier équipements qui contient tous les équipements de la voiture.
Chaque équipement est décrit avec un fichier au format [markdown](http://daringfireball.net/projects/markdown/).

Par exemple, ``equipements/roue.md` décrit la roue de la delorean.

### Ajouter un équipement

Pour ajouter un équipement, il faut ajouter le fichier correspondant dans le
dossier ``equipements`.

### Améliorer un équipement / corriger un bug

Pour améliorer un équipement, il faut faire la modification dans le fichier de
l'équipement.

## Bonus: le site généré

En **local** ou sur **gitpod**, pour générer le site:

```bash
# pour installer mkdocs - a faire une fois
make init

# pour générer le site
make

# pour servir le site sur le port 8000
# ouvrir ensuite firefox à l'url http://localhost:8000
# ou sur votre instance gitpod dont l'url est affichée 
# dans le Remote Explorer de la barre latérale.
make serve
```

Sur le serveur **gitlab**, le site est déployé à l'adresse:
https://infozesk.gitlab.io/{SUBGROUP}/{PROJECT}

Par exemple:

- https://infozesk.gitlab.io/formation-git/delorean
- https://infozesk.gitlab.io/formation-git/2022_iut/delo-01
